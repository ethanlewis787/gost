package main

import (
	"fmt"

	"github.com/russross/blackfriday"
)

func main() {
	output := blackfriday.MarkdownCommon([]byte("### Hello"))

	fmt.Println(string(output))

}
